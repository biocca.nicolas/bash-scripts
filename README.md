# Bash scripts

Some useful script I've been using in a regular basis.

## List of scripts

 - `image2video`: video generator utility from a set of numebered images.  

### image2video

Compile a set of sequential (ascendent) ordered images into an animation. 

```text
options:
  -h   | --help                 print the usage
  -wxh | --width-x-height       specify image size (width x height) [pixels]
  -o   | --output-video         name of output video file (default = video)
  -i   | --image-files          name of images files as a patternt (for exameple: image.%04d.png)
  -fr  | --frames-per-second    frames per second (default = 10)

Creates a video files from a sequence of PNG images
- A pattern like 'image.%04d.png' will expect files image.0000.png, image.0001.png, etc.
```

